﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Indicator : MonoBehaviour
{
    //индикатор, который выскакивает при увеличении или уменьшении здоровья персонажа
    void Start()
    {
        StartCoroutine(Indicat());
    }
    IEnumerator Indicat()
    {
        yield return new WaitForSeconds(0.3f);
        Destroy(gameObject);
    }
}
