﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Swipes : MonoBehaviour, IDragHandler, IBeginDragHandler
{
    public Rigidbody rg;
    public Animator anim;
    public Camera mainCamera;
    public GameObject hp, instruction;
    private bool going, finish, right, left;
    private float xPos = -4.9f, cHP;
    private float[] allPos = new float[4] {-17.8f, -4.9f, 6.4f, 19f}; //возможные позиции персонажа относительно координаты X

    void Update()
    {
        finish = GameObject.FindGameObjectWithTag("Character").GetComponent<Character>().finish;
        cHP = GameObject.FindGameObjectWithTag("Character").GetComponent<Character>().hp;
        right = GameObject.FindGameObjectWithTag("Character").GetComponent<Character>().right;
        left = GameObject.FindGameObjectWithTag("Character").GetComponent<Character>().left;
        //перемещаем персонажа
        if (going && cHP > 0)
        {
           rg.MovePosition(new Vector3(xPos, rg.transform.position.y, rg.transform.position.z + 0.9f));
           hp.transform.position = new Vector3(xPos, hp.transform.position.y, hp.transform.position.z);
        }
        if (!finish)
        {
            mainCamera.transform.position = new Vector3(mainCamera.transform.position.x, mainCamera.transform.position.y, rg.transform.position.z - 55);
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        
       
    }
    //считываем свайпы по экрану
    public void OnBeginDrag(PointerEventData eventData)
    {
        going = true;
        instruction.SetActive(false);
        anim.SetBool("Run", true);
        if (eventData.delta.x > 0 && right)
        {
            for (int i = allPos.Length - 1; i >= 0; i--)
            {
                if (xPos == allPos[i] && i + 1 != allPos.Length)
                {
                    xPos = allPos[i + 1];
                }
            }
        }
        else if (eventData.delta.x < 0 && left)
        {
            for (int i = 0; i < allPos.Length; i++)
            {
                if (xPos == allPos[i] && i - 1 >= 0)
                {
                    xPos = allPos[i - 1];
                }
            }
        }
    }
}
