﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Walls : MonoBehaviour
{
    public TextMesh hpStat;
    public float hp;
    private int colTime = 1;
  
    void Update()
    {
        //если здоровье стены равно или меньше 0, то убираем из виду стену(сделан костыль, т.к. при уничтожении стены анимация персонажа ломалась)
        if (hp <= 0)
        {
            gameObject.transform.position = new Vector3 (0, -1000, 0);
        }
        hpStat.text = hp.ToString();
    }
    //пока есть коллизии с персонажем, уменьшаем у стены здоровье на 1 единицу раз в 6 кадров
    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.CompareTag("Character"))
        {
            colTime--;
            if (colTime == 0)
            {
                hp--;
                colTime = 6;
            }
        }
    }
}
