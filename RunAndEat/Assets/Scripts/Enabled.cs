﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enabled : MonoBehaviour
{
    private Button btn;
    
    //считываем разблокирован ли уровень или нет
    void Start()
    {
        int tag;
        btn = GetComponent<Button>();
        tag = Convert.ToInt32(btn.gameObject.tag);
        if (PlayerPrefs.GetString("Level" + tag) != "Unlock")
        {
            btn.interactable = false;
        }
        else if (PlayerPrefs.GetString("Level" + tag) == "Unlock")
        {
            btn.interactable = true;
        }
    }
}
