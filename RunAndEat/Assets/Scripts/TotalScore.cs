﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TotalScore : MonoBehaviour
{
    private Text totalScore;
    private int sum;

    //суммируем результаты всех уровней и записываем в итог
    void Start()
    {
        totalScore = GetComponent<Text>();
        sum = PlayerPrefs.GetInt("ScoreLevel1") + PlayerPrefs.GetInt("ScoreLevel2") + PlayerPrefs.GetInt("ScoreLevel3");
        PlayerPrefs.SetString("TotalScore", totalScore.text);
        totalScore.text = "Total score: " + sum.ToString();
    }

}
