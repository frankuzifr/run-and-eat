﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Character : MonoBehaviour
{
    public TextMesh hpStat;
    public Text scoreText, levelScore1, levelScore2;
    public Canvas loseCanvas, winCanvas;
    private Animator anim;
    public GameObject belly;
    public GameObject[] indic;
    public AudioSource eating, win;
    public float hp;
    private int colTime = 1, score, maxScore;
    public bool finish, right = true, left = true;

    void Start()
    {
        anim = GetComponent<Animator>();
    }
    
    void Update()
    {
        if (hp <= 0)
        {
            anim.SetBool("Die", true);
            maxScore = score > PlayerPrefs.GetInt("ScoreLevel" + (SceneManager.GetActiveScene().buildIndex - 1)) ?  //записываем наивысший результат по данном уровню при проигрыше
                       score : PlayerPrefs.GetInt("ScoreLevel" + (SceneManager.GetActiveScene().buildIndex - 1));
            PlayerPrefs.SetInt("ScoreLevel" + (SceneManager.GetActiveScene().buildIndex - 1), maxScore); //запоминаем наивысший результат, чтобы он сохранился и после перезапуска игры
            gameObject.GetComponent<BoxCollider>().size = new Vector3(3.27f, 6, 7); //увеличиваем бокс коллайдер персонажа, чтобы при анимации падения, голова в сцену не уходила
            loseCanvas.gameObject.SetActive(true);
            scoreText.gameObject.SetActive(false);
        }
        else if (hp >= 10)
        {
            belly.SetActive(true); 
        }
        else if(hp < 10)
        {
            belly.SetActive(false);
        }
        hpStat.text = hp.ToString();
        scoreText.text = "Score: " + score.ToString();
        levelScore1.text = "Score: " + score.ToString();
        levelScore2.text = "Score: " + score.ToString();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Finish"))
        {
            win.Play();
            score += 50;
            finish = true;
            hpStat.gameObject.SetActive(false);
            maxScore = score > PlayerPrefs.GetInt("ScoreLevel" + (SceneManager.GetActiveScene().buildIndex - 1)) ? 
                       score : PlayerPrefs.GetInt("ScoreLevel" + (SceneManager.GetActiveScene().buildIndex - 1)); //записываем наивысший результат по данному уровню при финише
            PlayerPrefs.SetInt("ScoreLevel" + (SceneManager.GetActiveScene().buildIndex - 1), maxScore);
            scoreText.gameObject.SetActive(false);
            winCanvas.gameObject.SetActive(true);
            PlayerPrefs.SetString("Level" + SceneManager.GetActiveScene().buildIndex , "Unlock"); //открываем следующий уровень
            PlayerPrefs.SetString("Level" + (SceneManager.GetActiveScene().buildIndex - 1), "Unlock"); //оставляем открытым пройденный уровень
        }
    }
    private void OnCollisionStay(Collision collision)
    {
        //при коллизии со стеной, уменьшаем здоровье персонажа на 1 ед. раз в 6 кадров и уменьшаем размер персонажа и его живота
        if (collision.gameObject.CompareTag("Wall"))
        {
            colTime--;
            anim.SetBool("Push", true);
            if (colTime == 0)
            {
                hp--;
                Instantiate(indic[1], new Vector3(transform.position.x + 53, transform.position.y + 30, transform.position.z + 14), Quaternion.identity);
                score++;
                transform.localScale = transform.localScale - new Vector3(0.1f, 0.1f, 0.1f);
                belly.transform.localScale = belly.transform.localScale - new Vector3(3f, 0.1f, 3f);
                colTime = 6;
            }
        }
    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Wall"))
        {
            anim.SetBool("Push", false);
            colTime = 1;
        }
    }

    //если наступили на еду (мороженка, бургер, торт), здоровье персонажа увеличивается на 1 ед. и размер персонажа увеличивается
    //если наступили на броколь, здоровье персонажа уменьшается на 10 ед. и размер персонажа уменьшается
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Food"))
        {
            eating.Play();
            hp++;
            score++;
            transform.localScale = transform.localScale + new Vector3(0.1f, 0.1f, 0.1f);
            belly.transform.localScale = belly.transform.localScale + new Vector3(3f, 0.1f, 3f);
            Instantiate(indic[0], new Vector3(transform.position.x + 53, transform.position.y + 30, transform.position.z + 14), Quaternion.identity);
            Destroy(other.gameObject);
            
        }
        if (other.gameObject.CompareTag("Broccol"))
        {
            eating.Play();
            hp -= 10;
            score += 10;
            transform.localScale = transform.localScale - new Vector3(1f, 1f, 1f);
            belly.transform.localScale = belly.transform.localScale - new Vector3(30f, 1f, 30f);
            Instantiate(indic[2], new Vector3(transform.position.x + 53, transform.position.y + 30, transform.position.z + 14), Quaternion.identity);
            Destroy(other.gameObject);
        }
    }

    //ограничиваем движение влево или вправо, если между нашей позицией и конечной позицией есть стена
    private void OnTriggerStay(Collider other)
    {
        if(other.gameObject.CompareTag("Block"))
        {
            if (other.transform.position.x > transform.position.x)
            {
                right = false;
            }
            if (other.transform.position.x < transform.position.x)
            {
                left = false;
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Block"))
        {
            right = true;
            left = true;
        }
    }



}
