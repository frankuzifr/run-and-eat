﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class Music : MonoBehaviour
{
    public AudioMixerGroup Mixer;
    public bool musicPlaying = true;
    public Sprite[] musicSprites;
    public Button musicButton;

    private void Start()
    {
        if (PlayerPrefs.GetString("music") == "off")
        {
            Mixer.audioMixer.SetFloat("MasterVolume", -80);
            GetComponent<Image>().sprite = musicSprites[1];
            musicPlaying = false;
        }
        else if (PlayerPrefs.GetString("music") == "on")
        {
            Mixer.audioMixer.SetFloat("MasterVolume", 0);
            GetComponent<Image>().sprite = musicSprites[0];
            musicPlaying = true;
        }
    }

    public void MusicBtn()
    {

        if (musicPlaying)
        {
            Mixer.audioMixer.SetFloat("MasterVolume", -80);
            PlayerPrefs.SetString("music", "off");
            musicPlaying = false;
            GetComponent<Image>().sprite = musicSprites[1];
        }
        else
        {
            Mixer.audioMixer.SetFloat("MasterVolume", 0);
            PlayerPrefs.SetString("music", "on");
            musicPlaying = true;
            GetComponent<Image>().sprite = musicSprites[0];
        }
    }
}
